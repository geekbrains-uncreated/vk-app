package com.uncreated.vkapp.login;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.uncreated.vkapp.R;

public class LogInActivity extends AppCompatActivity {

    private InputLayoutListener mEmailListener;
    private InputLayoutListener mPasswordListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        initUI();
    }

    private void initUI() {
        TextInputLayout emailInputLayout = findViewById(R.id.email_input_layout);
        EditText emailEditText = findViewById(R.id.email_edit_text);

        TextInputLayout passwordInputLayout = findViewById(R.id.password_input_layout);
        EditText passwordEditText = findViewById(R.id.password_edit_text);

        Button button = findViewById(R.id.login_button);
        button.setOnClickListener(this::onLogin);

        String emailError = getResources().getString(R.string.email_error);
        String passwordError = getResources().getString(R.string.password_error);

        mEmailListener = new InputLayoutListener(emailInputLayout, emailEditText, emailError,
                text -> text.contains("@") && text.contains("."));

        mPasswordListener = new InputLayoutListener(passwordInputLayout, passwordEditText,
                passwordError, text -> text.length() > 0);
        mPasswordListener.setOnActionGoListener(this::onLogin);
    }

    public void onLogin(View v) {
        if (mEmailListener.isErrorHidden() && mPasswordListener.isErrorHidden()) {
            Toast.makeText(this, "Correct email & password", Toast.LENGTH_SHORT).show();
        }
    }
}
