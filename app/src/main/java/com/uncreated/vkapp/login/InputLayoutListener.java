package com.uncreated.vkapp.login;

import android.support.design.widget.TextInputLayout;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

class InputLayoutListener {
    private TextInputLayout mTextInputLayout;
    private EditText mEditText;
    private String mErrorMessage;
    private Validator mValidator;
    private OnActionGoListener mOnActionGoListener;

    InputLayoutListener(TextInputLayout textInputLayout, EditText editText,
                        String errorMessage, Validator validator) {
        mTextInputLayout = textInputLayout;
        mEditText = editText;
        mErrorMessage = errorMessage;
        mValidator = validator;

        editText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_GO || actionId == EditorInfo.IME_ACTION_NEXT) {
                boolean valid = isErrorHidden();
                if (valid && actionId == EditorInfo.IME_ACTION_GO) {
                    mOnActionGoListener.onActionGo(mEditText);
                }
                return !valid;
            }
            return false;
        });
    }

    boolean isErrorHidden() {
        if (!mValidator.isValid(mEditText.getText().toString())) {
            mTextInputLayout.setError(mErrorMessage);
            return false;
        } else {
            mTextInputLayout.setError(null);
            return true;
        }
    }

    void setOnActionGoListener(OnActionGoListener onActionGoListener) {
        mOnActionGoListener = onActionGoListener;
    }

    public interface Validator {
        boolean isValid(String text);
    }

    public interface OnActionGoListener {
        void onActionGo(EditText editText);
    }
}
