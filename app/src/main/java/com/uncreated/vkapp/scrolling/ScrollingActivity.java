package com.uncreated.vkapp.scrolling;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.uncreated.vkapp.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ScrollingActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.fab)
    FloatingActionButton mFab;

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    private ArrayList<UserImageInformation> mImageList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);

        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);
        initRecycler();
    }

    private void initRecycler() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);
        mImageList = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            mImageList.add(new UserImageInformation("Photo №" + i, R.drawable.les));
        }

        CardRecyclerAdapter cardRecyclerAdapter = new CardRecyclerAdapter(mImageList, position -> {
            mImageList.remove(position);
            mRecyclerView.getAdapter().notifyDataSetChanged();
        });
        mRecyclerView.setAdapter(cardRecyclerAdapter);
        cardRecyclerAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.fab)
    protected void addUserImage(View v) {
        UserImageInformation userImageInformation = new UserImageInformation(
                "Photo №" + mImageList.size(), R.drawable.les);
        mImageList.add(userImageInformation);
        mRecyclerView.getAdapter().notifyItemChanged(mImageList.size() - 1);
        Snackbar.make(v, "User image added\"" + userImageInformation.getTitle() + "\"",
                Snackbar.LENGTH_LONG).setAction("Action", null).show();
    }
}
