package com.uncreated.vkapp.scrolling;

public class UserImageInformation {
    private String mTitle;
    private int mImageId;

    public UserImageInformation(String title, int imageId) {
        mTitle = title;
        mImageId = imageId;
    }

    public String getTitle() {
        return mTitle;
    }

    public int getImageId() {
        return mImageId;
    }
}
