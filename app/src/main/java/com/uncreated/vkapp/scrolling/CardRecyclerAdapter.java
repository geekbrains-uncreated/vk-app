package com.uncreated.vkapp.scrolling;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.uncreated.vkapp.R;

import java.util.List;

public class CardRecyclerAdapter extends RecyclerView.Adapter<CardRecyclerAdapter.CardViewHolder> {

    private List<UserImageInformation> mImageList;
    private OnItemLongClickListener mOnItemLongClickListener;

    public CardRecyclerAdapter(List<UserImageInformation> imageList,
                               OnItemLongClickListener onItemLongClickListener) {
        mImageList = imageList;
        mOnItemLongClickListener = onItemLongClickListener;
    }

    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item, parent,
                false);
        return new CardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewHolder holder, int position) {
        Log.d("RecyclerTag", "Bind #" + position);
        UserImageInformation userImageInformation = mImageList.get(position);
        holder.mTextView.setText(userImageInformation.getTitle());
        holder.mImageView.setImageResource(userImageInformation.getImageId());
        holder.itemView.setOnLongClickListener(v -> {
            mOnItemLongClickListener.onItemLongClick(position);
            return true;
        });
    }

    @Override
    public int getItemCount() {
        return mImageList.size();
    }

    interface OnItemLongClickListener {
        void onItemLongClick(int position);
    }

    class CardViewHolder extends RecyclerView.ViewHolder {

        TextView mTextView;
        ImageView mImageView;

        CardViewHolder(View itemView) {
            super(itemView);
            mTextView = itemView.findViewById(R.id.text_view);
            mImageView = itemView.findViewById(R.id.image_view);
        }
    }
}
