package com.uncreated.vkapp.main;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.uncreated.vkapp.R;
import com.uncreated.vkapp.login.LogInActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    private static final String GALLERY_TAG = "galleryFragment";
    private static final String SLIDESHOW_TAG = "slideshowFragment";
    private static final String SETTINGS_TAG = "settingsFragment";

    //debug
    final String name = "Ivan Petrov";
    final String email = "ivan.petrov@mail.ru";
    ColorStateList mColorStateList = new ColorStateList(new int[][]{
            new int[]{R.attr.state_disabled},
            new int[]{android.R.attr.state_enabled},
            new int[]{R.attr.state_unchecked},
            new int[]{android.R.attr.state_pressed}
    }, new int[]{
            Color.RED,
            Color.GREEN,
            Color.BLUE,
            Color.BLACK
    });

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawer;
    @BindView(R.id.nav_view)
    NavigationView mNavigationView;

    private NavigationView.OnNavigationItemSelectedListener mOnItemSelectedListener;
    private FragmentManager mFragmentManager;
    private GalleryFragment mGalleryFragment;
    private SlideshowFragment mSlideshowFragment;
    private SettingsFragment mSettingsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        initUi();
        initFragments(savedInstanceState);
    }

    private void initFragments(Bundle savedInstanceState) {
        mFragmentManager = getSupportFragmentManager();
        if (savedInstanceState == null) {
            mOnItemSelectedListener.onNavigationItemSelected(
                    mNavigationView.getMenu().findItem(R.id.nav_gallery));
            mNavigationView.setCheckedItem(R.id.nav_gallery);
        } else {
            mGalleryFragment = (GalleryFragment) mFragmentManager.findFragmentByTag(GALLERY_TAG);
            mSlideshowFragment = (SlideshowFragment) mFragmentManager.findFragmentByTag(SLIDESHOW_TAG);
            mSettingsFragment = (SettingsFragment) mFragmentManager.findFragmentByTag(SETTINGS_TAG);
        }
    }

    private void initUi() {
        setSupportActionBar(mToolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.addDrawerListener(toggle);
        toggle.syncState();

        mOnItemSelectedListener = getNavigationItemSelectedListener();
        mNavigationView.setNavigationItemSelectedListener(mOnItemSelectedListener);

        View headerView = mNavigationView.getHeaderView(0);
        TextView nameTextView = headerView.findViewById(R.id.tv_nav_header_name);
        nameTextView.setText(name);
        TextView emailTextView = headerView.findViewById(R.id.tv_nav_header_email);
        emailTextView.setText(email);

        mNavigationView.setItemTextColor(mColorStateList);
        mNavigationView.setItemIconTintList(mColorStateList);
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        } else {
            startActivity(new Intent(this, LogInActivity.class));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.menu_reload:
                Toast.makeText(this, "Clicked toolbar reload", Toast.LENGTH_SHORT).show();
                break;
            case R.id.menu_options:
                Toast.makeText(this, "Clicked toolbar options", Toast.LENGTH_SHORT).show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private NavigationView.OnNavigationItemSelectedListener getNavigationItemSelectedListener() {
        return item -> {
            int id = item.getItemId();

            if (id == R.id.nav_gallery) {
                if (mGalleryFragment == null) {
                    mGalleryFragment = new GalleryFragment();
                }
                switchFragment(mGalleryFragment, GALLERY_TAG);
            } else if (id == R.id.nav_slideshow) {
                if (mSlideshowFragment == null) {
                    mSlideshowFragment = new SlideshowFragment();
                }
                switchFragment(mSlideshowFragment, SLIDESHOW_TAG);
            } else if (id == R.id.nav_settings) {
                if (mSettingsFragment == null) {
                    mSettingsFragment = new SettingsFragment();
                }
                switchFragment(mSettingsFragment, SETTINGS_TAG);
            }

            mDrawer.closeDrawer(GravityCompat.START);
            return true;
        };
    }

    private void switchFragment(Fragment fragment, String tag) {
        mFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment, tag)
                .commit();
    }
}
