package com.uncreated.vkapp.main;

import android.support.v4.app.Fragment;
import android.util.Log;

public class BaseFragment extends Fragment {

    public BaseFragment() {
        Log.d("FragmentTag", getClass().getSimpleName() + " created");
    }
}
