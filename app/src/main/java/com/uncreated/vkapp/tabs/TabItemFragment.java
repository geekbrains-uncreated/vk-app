package com.uncreated.vkapp.tabs;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.uncreated.vkapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TabItemFragment extends Fragment {

    public static final String ARG_TITLE_KEY = "argTitle";

    @BindView(R.id.text_view)
    TextView mTextView;

    public TabItemFragment() {
    }

    public static TabItemFragment newInstance(String title) {
        TabItemFragment tabItemFragment = new TabItemFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE_KEY, title);
        tabItemFragment.setArguments(args);
        return tabItemFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_item, container, false);
        ButterKnife.bind(this, view);
        Bundle args = getArguments();
        if (args != null) {
            String title = args.getString(ARG_TITLE_KEY);
            mTextView.setText(title);
        }
        return view;
    }

}
