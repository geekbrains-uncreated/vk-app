package com.uncreated.vkapp.tabs;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.uncreated.vkapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BottomNavigationActivity extends AppCompatActivity {

    @BindView(R.id.navigation)
    BottomNavigationView mBottomNavigationView;

    private TabItemFragment mFragment1;
    private TabItemFragment mFragment2;
    private TabItemFragment mFragment3;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            TabItemFragment fragment;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragment = mFragment1;
                    break;
                case R.id.navigation_dashboard:
                    fragment = mFragment2;
                    break;
                case R.id.navigation_notifications:
                    fragment = mFragment3;
                    break;
                default:
                    return false;
            }
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_navigation);

        ButterKnife.bind(this);

        initNavigation();
    }

    private void initNavigation() {
        mFragment1 = TabItemFragment.newInstance(getResources().getString(R.string.title_home));
        mFragment2 = TabItemFragment.newInstance(getResources().getString(R.string.title_dashboard));
        mFragment3 = TabItemFragment.newInstance(getResources().getString(R.string.title_notifications));

        mBottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        mBottomNavigationView.setSelectedItemId(R.id.navigation_home);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, TabsActivity.class));
    }
}
