package com.uncreated.vkapp.tabs;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.uncreated.vkapp.R;
import com.uncreated.vkapp.main.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TabsActivity extends AppCompatActivity {


    @BindView(R.id.tab_layout)
    TabLayout mTabLayout;

    @BindView(R.id.view_pager)
    ViewPager mViewPager;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    private GalleryPagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabs);

        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);

        initTabs();
    }

    private void initTabs() {
        mPagerAdapter = new GalleryPagerAdapter(getSupportFragmentManager());
        for (int i = 0; i < 10; i++) {
            String title = "Tab_" + i;
            mPagerAdapter.addFragment(TabItemFragment.newInstance(title), title);
        }
        mViewPager.setAdapter(mPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);

        for (int i = 0; i < 10; i++) {
            TabLayout.Tab tab = mTabLayout.getTabAt(i);
            if (tab != null) {
                if (i % 2 == 0) {
                    tab.setIcon(R.drawable.ic_menu_camera);
                } else {
                    tab.setIcon(R.drawable.ic_menu_gallery);
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.menu_reload:
                Toast.makeText(this, "Clicked toolbar reload", Toast.LENGTH_SHORT).show();
                break;
            case R.id.menu_options:
                Toast.makeText(this, "Clicked toolbar options", Toast.LENGTH_SHORT).show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
    }
}
